FROM debian:stable-slim

RUN apt-get update \
  && apt-get install -y socat \
  && apt-get upgrade -y \
  && apt-get clean

ENTRYPOINT ["socat"]
